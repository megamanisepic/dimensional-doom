# Dimensional Doom #
# Feeling the pain means fighting the pain

This is a incredibly hard modpack, which provides a very interesting late-game combined with a frustrating early-game and a somehow fascinating mid-game.
The stage of development is something like pre-alpha, but we are working a lot to complete the pack.
This pack includes 170 of the best mods, including some well-known such as Mekanism, Thaumcraft, IC2, Tinkers, Botania, Thermal*, Blood Magic, Avaritia,<br>
AE2, Nuclear Control and many, many more! Every mod in this pack is written really well, and this combination creates the ultimate hardcore modpack, <br>
which states to be harder than FTB Infinity Skyblock.
