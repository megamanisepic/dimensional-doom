//EXTRA UTILITIES
//Item Transfer Node nerf
recipes.remove(<ExtraUtilities:extractor_base>);
recipes.addShaped(<ExtraUtilities:extractor_base>,[[null,<minecraft:hopper>,null],[<ore:chipsetIron>,<ExtraUtilities:pipes>,<ore:chipsetIron>],
[<minecraft:coal_block>,<minecraft:furnace>,<minecraft:coal_block>]]);
//Fluid Transfer Node nerf
recipes.remove(<ExtraUtilities:extractor_base:6>);
recipes.addShaped(<ExtraUtilities:extractor_base:6>,[[null,<ExtraUtilities:extractor_base>,null],[<ore:blockLapis>,<minecraft:ender_pearl>,<ore:blockLapis>],[<ore:ingotInvar>,<minecraft:bucket>,<ore:ingotInvar>]]);
//World Interaction upgrade nerf
recipes.remove(<ExtraUtilities:nodeUpgrade:2>);
recipes.addShaped(<ExtraUtilities:nodeUpgrade:2>,[[<ore:dyeBlue>,<ore:blockLapis>,<ore:dyeBlue>],[<ore:blockLapis>,<ThermalFoundation:tool.pickaxeBronze>,<ore:blockLapis>],[<ore:dyeBlue>,<ore:blockLapis>,<ore:dyeBlue>]]);
//Transfer Pipe nerf
recipes.remove(<ExtraUtilities:pipes>);
recipes.addShaped(<ExtraUtilities:pipes> * 6,[[<ore:plateSteel>,<ore:plateSteel>,<ore:plateSteel>],[<minecraft:redstone_block>,<BuildCraft|Transport:item.buildcraftPipe.pipeitemswood>,<minecraft:redstone_block>],[<ore:plateSteel>,<ore:plateSteel>,<ore:plateSteel>]]);
