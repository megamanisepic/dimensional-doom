//VANILLA

//Wood Nerf
//Any non-vanilla wood has to be processed via machines
recipes.remove(<ore:plankWood>);
recipes.addShapeless(<minecraft:planks>*3,[<minecraft:log>]);
recipes.addShapeless(<minecraft:planks:1>*3, [<minecraft:log:1>]);
recipes.addShapeless(<minecraft:planks:2>*3,[<minecraft:log:2>]);
recipes.addShapeless(<minecraft:planks:3>*3,[<minecraft:log:3>]);
recipes.addShapeless(<minecraft:planks:4>*3,[<minecraft:log2>]);
recipes.addShapeless(<minecraft:planks:5>*3,[<minecraft:log2:1>]);

recipes.remove(<minecraft:stick>);
recipes.addShaped(<minecraft:stick> * 3, [[<ore:plankWood>], [<ore:plankWood>]]);

//Nerfing furnaces
recipes.remove(<TConstruct:FurnaceSlab>);
recipes.remove(<minecraft:furnace>);
recipes.addShaped(<minecraft:furnace>, [[<ore:compressedCobblestone1x>, <ore:cobblestone>, <ore:compressedCobblestone1x>], [<ore:cobblestone>, null, <ore:cobblestone>], [<ore:compressedCobblestone1x>, <ore:cobblestone>, <ore:compressedCobblestone1x>]]);
//recipes.addShaped(<TConstruct:FurnaceSlab>,[[minecraft:slab_

//Nerfing chests
recipes.remove(<minecraft:chest>);
recipes.addShaped(<minecraft:chest>, [[<ore:plankWood>, <ore:logWood>, <ore:plankWood>], [<ore:logWood>, <ore:nuggetIron>, <ore:logWood>], [<ore:plankWood>, <ore:logWood>, <ore:plankWood>]]);

//Disabling wood shears and stone shears
recipes.remove(<ThermalFoundation:tool.shearsWood>);
recipes.remove(<ThermalFoundation:tool.shearsStone>);

//Nerfing torches
recipes.remove(<minecraft:torch>);
recipes.addShaped(<minecraft:torch>, [[<ore:gemCoal>], [<ore:stickWood>]]);

//Finally removing the bucket
recipes.remove(<minecraft:bucket>);
recipes.addShaped(<minecraft:bucket>,[[null,null,null],[<ore:plateSteel>,null,<ore:plateSteel>],[null,<ore:plateSteel>,null]]);

//TINKERS' CONSTRUCT

//Tinkers' Construct Crafting
recipes.remove(<TConstruct:CraftingStation>);
recipes.addShaped(<TConstruct:CraftingStation>, [[<minecraft:crafting_table>, <ore:crafterWood>, <ore:crafterWood>], [<minecraft:stick>, null, <ore:stickWood>], [<ore:stickWood>, null, <ore:stickWood>]]);


//IMMERSIVE ENGINEERING

//Nerfing iron rods
recipes.remove(<ImmersiveEngineering:material:14>);
recipes.addShaped(<ImmersiveEngineering:material:14>,[[<minecraft:iron_ingot>],[<minecraft:iron_ingot>]]);
//Nerfing coke oven
recipes.remove(<ImmersiveEngineering:stoneDecoration:1>);
recipes.addShaped(<ImmersiveEngineering:stoneDecoration:1>,[[<minecraft:clay>,<minecraft:brick_block>,<minecraft:clay>],[<minecraft:brick_block>,<ExtraUtilities:cobblestone_compressed:14>,<minecraft:brick_block>],[<minecraft:clay>,<minecraft:brick_block>,<minecraft:clay>]]);
//Nerfing dat Hammah
recipes.remove(<ImmersiveEngineering:tool>);
recipes.addShaped(<ImmersiveEngineering:tool>, [[null, <ore:blockIron>, null], [null, <ore:stickIron>, <ore:blockIron>], [<ImmersiveEngineering:material>, null, null]]);
//Nerfing plates
recipes.remove(<ore:plateSteel>);
recipes.addShapeless(<ImmersiveEngineering:metal:38>,[<ImmersiveEngineering:tool>,<ore:blockSteel>]);
//FORGE MICROBLOCKS

//Nerfing the stone saw
recipes.remove(<ForgeMicroblock:sawStone>);
recipes.addShaped(<ForgeMicroblock:sawStone>, [[<ImmersiveEngineering:material:14>, <ImmersiveEngineering:material:14>, <ImmersiveEngineering:material:14>], [<ore:stickWood>, <ore:stone>, <ore:stickWood>]]);

//EXTRA UTILITIES
//re-adding mini chest recipe
recipes.addShapeless(<minecraft:chest>, [<ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>, <ExtraUtilities:chestMini>]);

//EX NIHILO

//Ex Nihilo Barrel
mods.exnihilo.Composting.removeRecipe(<minecraft:leaves>);

//Ex Nihilo Crafting
recipes.remove(<exnihilo:crucible_unfired>); //Nerfing Crucible
recipes.addShaped(<exnihilo:crucible_unfired>, [[<exnihilo:porcelain>, null, <exnihilo:porcelain>], [<minecraft:clay>, null, <minecraft:clay>], [<exnihilo:porcelain>, <minecraft:clay>, <exnihilo:porcelain>]]);

recipes.remove(<exnihilo:barrel>); //Nerfing Barrel
recipes.addShaped(<exnihilo:barrel>, [[<ore:plankWood>, null, <ore:plankWood>], [<ore:logWood>, null, <ore:logWood>], [<ore:plankWood>, <ore:logWood>, <ore:plankWood>]]);
recipes.remove(<exnihilo:barrel:5>); //Barrels have to be chiseled
recipes.remove(<exnihilo:barrel:4>);
recipes.remove(<exnihilo:barrel:3>);
recipes.remove(<exnihilo:barrel:2>);
recipes.remove(<exnihilo:barrel:1>);
//Sieve Nerf
recipes.remove(<exnihilo:mesh>);
recipes.addShaped(<exnihilo:mesh>,[[<minecraft:wool>,<minecraft:wool>],[<minecraft:wool>,<minecraft:wool>]]);
//Nerfing Ex Compressum heavy sieve
recipes.remove(<excompressum:heavySieve:*>);
recipes.addShaped(<excompressum:heavySieve>,[[<minecraft:log>,<excompressum:heavySilkMesh>,<minecraft:log>],[<minecraft:log>,<excompressum:heavySilkMesh>,<minecraft:log>],[<ImmersiveEngineering:material:14>,<exnihilo:sifting_table>,<ImmersiveEngineering:material:14>]]);
//Nerfing all those hammers
recipes.remove(<exnihilo:hammer_stone>);
recipes.remove(<exnihilo:hammer_wood>);
recipes.remove(<exnihilo:hammer_gold>);
recipes.remove(<exastris:manaHammer>);
recipes.remove(<exnihilo:hammer_diamond>);
recipes.remove(<exnihilo:hammer_iron>);
recipes.addShaped(<exnihilo:hammer_stone>, [[null, <ore:stone>, null], [null, <minecraft:bone>, <ore:stone>], [<minecraft:bone>, null, null]]);
recipes.addShaped(<exnihilo:hammer_wood>, [[null, <ore:logWood>, null], [null, <minecraft:bone>, <ore:logWood>], [<minecraft:bone>, null, null]]);
recipes.addShaped(<exnihilo:hammer_gold>, [[null, <ore:blockGold>, null], [null, <minecraft:bone>, <ore:blockGold>], [<minecraft:bone>, null, null]]);
recipes.addShaped(<exastris:manaHammer>, [[null, <Botania:storage>, null], [null, <ore:livingwoodTwig>, <Botania:storage>], [<ore:livingwoodTwig>, null, null]]);
recipes.addShaped(<exnihilo:hammer_diamond>, [[null, <minecraft:diamond_block>, null], [null, <minecraft:bone>, <minecraft:diamond_block>], [<minecraft:bone>, null, null]]);
recipes.addShaped(<exnihilo:hammer_iron>, [[null, <ore:blockIron>, null], [null, <minecraft:bone>, <ore:blockIron>], [<minecraft:bone>, null, null]]);


//InductrialCraft 2
//Nerfing the forge hammer
var forgeHammer= <IC2:itemToolForgeHammer>;
recipes.remove(forgeHammer);
recipes.addShaped(forgeHammer,[[<ore:plateSteel>,<excompressum:compressedHammerIron>,<ore:plateSteel>],[null,<ImmersiveEngineering:tool>,null],[null,<ImmersiveEngineering:material:14>,null]]);
