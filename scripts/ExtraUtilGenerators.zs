//EXTRA UTILITIES
//Nerfing the pink generator - it will be the first one available
recipes.remove(<ExtraUtilities:generator:*>);
recipes.addShaped(<ExtraUtilities:generator:9>,[[<minecraft:wool:6>,<ExtraUtilities:extractor_base>,<minecraft:wool:6>],[<minecraft:furnace>,<minecraft:chest>,<minecraft:furnace>],[<ore:plateSteel>,<ore:gearDiamond>,<ore:plateSteel>]]);
//Nerfing the lava generator to doom
recipes.addShaped(<ExtraUtilities:generator:2>,[[null,<ExtraUtilities:generator:9>,null],[<minecraft:water_bucket>.noReturn(),<ProjRed|Core:projectred.core.part:15>,<minecraft:water_bucket>.noReturn()],[<ore:blockGold>,<ThermalExpansion:Tank:1>,<ore:blockGold>]]);
